package test.admin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import controller.Controller;
import model.admin.Pair;
import model.admin.products.BuyObject;
import model.admin.products.Instructor;
import model.admin.products.Season;

/**
 * Junit tests for management functions.
 */
public class ManagementTest {

    private static final Controller CON = Controller.getController();
    private static final int LAST = 12;

    /**
     * Test sets with operation.
     */
    @Test
    public void testOperationSet() {
        //Reset application to work only on demo operations
        CON.resetApplication();
        //Check presence and size in all operations key set
        assertFalse(CON.getAllOperations().contains(0));
        assertEquals(10, CON.getAllOperations().size());
        //Check presence and size in user operations key set
        assertTrue(CON.getUserOperations("anto63").contains(1));
        assertNotEquals(2, CON.getUserOperations("robi85").size());
        //Check presence and size in type operations key set
        assertFalse(CON.getTypeOperations("Noleggio Articoli").contains(1));
        assertNotEquals(1, CON.getTypeOperations("Acquisto Articoli").size());
        //Check presence and size in user and type operations key set
        assertTrue(CON.getUserAndTypeOperations("mario78", "Acquisto Skipass").contains(3));
        assertEquals(0, CON.getUserAndTypeOperations("laura90", "Deposito Articoli").size());
    }

    /**
     * Test sets with operation types.
     */
    @Test
    public void testOperationTypes() {
        //Reset application to work only on demo operations
        CON.resetApplication();
        //Check size and presence in types set of all operations
        assertNotEquals(3, CON.getOperationTypes(CON.getAllOperations()).size());
        assertTrue(CON.getOperationTypes(CON.getAllOperations()).contains("Acquisto Articoli"));
        //Check size and presence in types set of user operations
        assertEquals(0, CON.getOperationTypes(CON.getUserOperations("tommi96")).size());
        assertTrue(CON.getOperationTypes(CON.getUserOperations("anto63")).contains("Prenotazione Maestro"));
    }

    /**
     * Test functions that calculate operations gain.
     */
    @Test
    public void testOperationsGain() {
        //Reset application to work only on demo operations
        CON.resetApplication();
        //Check function that calculate the gain of a single operation
        assertNotEquals(new Pair<>("0,00", "0,00"), CON.getOperationGain(1));
        assertEquals(new Pair<>("71,25", "61,25"), CON.getOperationGain(2));
        assertNotEquals(new Pair<>("null", "null"), CON.getOperationGain(3));
        //Check function that calculate the total gain of the store
        assertEquals("1.122,78", CON.getTotalGain().getX());
        assertNotEquals("1.122,78", CON.getTotalGain().getY());
    }

    /**
     * Test function that complete cart operations.
     */
    @Test
    public void testOperationsComplete() {
        //Reset application to start work only on demo operations
        CON.resetApplication();
        //Added two products to cart and called complete operations function
        CON.addBuy(BuyObject.SKIS, "2");
        CON.addInstructor(Instructor.SNOWBOARD_3HOURS, "1", Season.HIGH_SEASON);
        CON.loginUser("robi85", "85robi");
        CON.completeOperations();
        //Check operations correctly completed
        assertEquals(LAST, CON.getAllOperations().size());
        assertEquals("Prenotazione Maestro", CON.getOperations().get(LAST).getY().getDescription());
        assertNotEquals("ciao", CON.getOperations().get(LAST).getX());
        //Reset application to deleted the memorized operations and check the reset
        CON.resetApplication();
        assertNotEquals(LAST, CON.getAllOperations().size());
    }
 
}
