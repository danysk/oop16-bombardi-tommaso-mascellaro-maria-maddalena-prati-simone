package test.admin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import controller.Controller;
import model.admin.products.Instructor;
import model.admin.products.BuyObject;
import model.admin.products.RentObject;
import model.admin.products.Season;
import model.admin.products.Skipass;

/**
 * Junit tests for store functions.
 */
public class StoreTest {

    private static final Controller CON = Controller.getController();

    /**
     * Test sets with all available products. 
     */
    @Test
    public void testProductsSet() {
        //Check presence, order and size in buy objects returned set
        assertTrue(CON.getBuyObjects().contains(BuyObject.SKI_POLES));
        assertNotEquals(BuyObject.SNOW_PANTS, CON.getBuyObjects().iterator().next());
        assertEquals(BuyObject.values().length, CON.getBuyObjects().size());
        //Check presence, order and size in rent and storage objects returned set
        assertTrue(CON.getRentAndStorageObjects().contains(RentObject.SNOWBOARD));
        assertEquals(RentObject.HELMET, CON.getRentAndStorageObjects().iterator().next());
        assertNotEquals(1, CON.getRentAndStorageObjects().size());
        //Check presence, order and size in instructors returned set
        assertTrue(CON.getInstructors().contains(Instructor.SNOWBOARD_2HOURS));
        assertNotEquals(Instructor.SKI_3HOURS, CON.getInstructors().iterator().next());
        assertNotEquals(0, CON.getInstructors().size());
        //Check presence, order and size in skipass returned set
        assertTrue(CON.getSkipass().contains(Skipass.FIVE_DAYS));
        assertEquals(Skipass.HALF_DAY, CON.getSkipass().iterator().next());
        assertEquals(Skipass.values().length, CON.getSkipass().size());
        //Check presence, order and size in seasons returned set
        assertTrue(CON.getSeasons().contains(Season.OFF_SEASON));
        assertEquals(Season.HIGH_SEASON, CON.getSeasons().iterator().next());
        assertNotEquals(2, CON.getSeasons().size());
    }

    /**
     * Test functions that find product type.
     */
    @Test
    public void testProductsSearch() {
        //Check the optional returned by find instructor function
        assertEquals(Optional.of(Instructor.SKI_3HOURS), CON.getInstructor("Maestro Di Sci (3 Ore)"));
        assertEquals(Optional.empty(), CON.getInstructor("Maestro"));
        //Check the optional returned by find skipass function
        assertNotEquals(Optional.of(Skipass.TWO_DAYS), CON.getSkipass("Una Settimana"));
        assertNotEquals(Optional.empty(), CON.getSkipass("Dieci Giorni"));
        //Check the optional returned by find season function
        assertEquals(Optional.of(Season.MID_SEASON), CON.getSeason("Febbraio-Marzo"));
        assertNotEquals(Optional.of(Season.HIGH_SEASON), CON.getSeason("Alta Stagione"));
    }

    /**
     * Test functions that return product price.
     */
    @Test
    public void testProductsPrice() {
        //Check buy price with wrong and correct inputs
        try {
            CON.getBuyPrice(BuyObject.GLOVES, "11");
        } catch (Exception e) {
            assertTrue(e.getClass().equals(IllegalArgumentException.class));
        }
        assertEquals("219,80", CON.getBuyPrice(BuyObject.SNOWBOARD_BOOTS, "2"));
        //Check rent price with wrong and correct inputs
        try {
            CON.getRentPrice(RentObject.HELMET, "1", "22", Season.OFF_SEASON);
        } catch (Exception e) {
            assertTrue(e.getClass().equals(IllegalArgumentException.class));
        }
        assertEquals("59,20", CON.getRentPrice(RentObject.SNOWBOARD_BOOTS, "2", "4", Season.MID_SEASON));
        //Check instructor price with wrong and correct inputs
        try {
            CON.getInstructorPrice(Instructor.SKI_2HOURS, "6", Season.MID_SEASON);
        } catch (Exception e) {
            assertTrue(e.getClass().equals(IllegalArgumentException.class));
        }
        assertNotEquals("10,00", CON.getInstructorPrice(Instructor.SKI_2HOURS, "1", Season.HIGH_SEASON));
        //Check skipass price with wrong and correct inputs
        try {
            CON.getSkipassPrice(Skipass.FIVE_DAYS, "15", Season.MID_SEASON);
        } catch (Exception e) {
            assertTrue(e.getClass().equals(IllegalArgumentException.class));
        }
        assertNotEquals("5,00", CON.getSkipassPrice(Skipass.ONE_DAY, "2", Season.OFF_SEASON));
        //Check storage price with wrong and correct inputs
        try {
            CON.getStoragePrice(RentObject.SKIS, "1", "not");
        } catch (Exception e) {
            assertTrue(e.getClass().equals(IllegalArgumentException.class));
        }
        assertEquals("5,00", CON.getStoragePrice(RentObject.SKIS, "1", "1"));
    }
}
