package view.panels.interfaces;

import javax.swing.JButton;
/**
 * 
 * Common interface for panelDetails.
 *
 */
public interface PanelDetails extends Panel {
    /**
     * delete object.
     */
    void resetObject();
    /**
     * add details.
     */
    void addDetails();
    /**
     * remove details.
     */
    void removeDetails();
    /**
     * 
     * @return button for panel cart
     */
    JButton getBtnCart();
}
