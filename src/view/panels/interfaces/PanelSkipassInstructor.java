package view.panels.interfaces;

import javax.swing.JButton;
/**
 * 
 * panel skipass interface.
 *
 */
public interface PanelSkipassInstructor  extends Panel {
    /**
     * 
     * @return button for panel cart
     */
    JButton getBtnCart();
}
