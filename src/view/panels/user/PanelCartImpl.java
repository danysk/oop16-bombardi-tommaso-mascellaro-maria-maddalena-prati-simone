package view.panels.user;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import model.admin.Pair;
import view.panels.FrameSize;
import view.panels.interfaces.PanelCart;

/**
 * 
 * panel cart class.
 *
 */
public class PanelCartImpl extends JPanel implements PanelCart {

    private static final long serialVersionUID = 1L;
    private final JButton btnRetMenu;
    private final JButton btnFinishOp;
    private final DefaultTableModel model;
    private final JLabel labPrice;
    private final JTextField text;
    private final JButton btnEmptyCart;
    private final JButton btnDeleteOp;
    private int count;
    //COLORE
    private static final int C1 = 171;
    private static final int C2 = 205;
    private static final int C3 = 239;

    private static final int COL1 = FrameSize.WIDTH.getValue() / 12;
    private static final int COL2 = FrameSize.WIDTH.getValue() / 4;
    private static final int COL3 = (int) (FrameSize.WIDTH.getValue() / 4.1);
    private static final int COL4 = FrameSize.WIDTH.getValue() / 7;
    private static final int LIM = (int) (FrameSize.HEIGHT.getValue() / 1.5);
    private static final int WC = FrameSize.WIDTH.getValue() / 4;
    private static final int WC1 = FrameSize.WIDTH.getValue() / 15;
    private static final int WC2 = FrameSize.WIDTH.getValue() / 20;
    private static final int WC3 = (int) (FrameSize.WIDTH.getValue() / 3.65);
    private static final int WC4 = (int) (FrameSize.WIDTH.getValue() / 2.8);
    private static final int WC5 = (int) (FrameSize.WIDTH.getValue() / 1.5);

    private static final int HC = FrameSize.HEIGHT.getValue() / 15;
    private static final int HC1 = FrameSize.HEIGHT.getValue() / 60;
    private static final int HC2 = FrameSize.HEIGHT.getValue() / 8;

    private static final int FONT1 = FrameSize.WIDTH.getValue() / 50;
    private static final int FONT2 = FrameSize.WIDTH.getValue() / 35;



    /**
     * panel cart constructor.
     */
    public PanelCartImpl() {
        super();
        this.count = 0;
        final JPanel panelSouth = new JPanel();
        final GridBagLayout layout = new GridBagLayout();
        final GridBagConstraints lim = new GridBagConstraints();
        this.setLayout(layout);
        //TABELLA CARRELLO
        this.model = new DefaultTableModel();
        model.addColumn("Num op."); 
        model.addColumn("Tipo op."); 
        model.addColumn("Dettaglio op.");
        model.addColumn("Costo op.");
        final JTable table = new JTable(model);
        table.setEnabled(false);
        table.getColumn("Num op.").setPreferredWidth(COL1);
        table.getColumn("Tipo op.").setPreferredWidth(COL2);
        table.getColumn("Dettaglio op.").setPreferredWidth(COL3);
        table.getColumn("Costo op.").setPreferredWidth(COL4);
        final JScrollPane sp = new JScrollPane(table);
        final Component c0 = sp;
        lim.gridx = 0;
        lim.gridy = 0;
        lim.weightx = 1;
        lim.weighty = 1;
        lim.ipadx = FrameSize.WIDTH.getValue();
        lim.ipady = LIM;
        lim.fill = GridBagConstraints.NONE;
        lim.anchor = GridBagConstraints.NORTH;
        layout.setConstraints(c0, lim);
        this.add(c0);
        //SFONDO
        final Color bluette = new Color(C1, C2, C3);
        this.setBackground(bluette);
        //OPERAZIONE DA ELIMINARE
        final JLabel lab = new JLabel("Operazione da eliminare");
        lab.setBounds(WC2, HC1, WC, HC);
        lab.setFont(new Font("Tahoma", Font.PLAIN,  FONT1));
        panelSouth.add(lab);
        this.text = new JTextField();
        this.text.setBounds(WC3, HC1, WC1, HC);
        panelSouth.add(text);
        this.btnDeleteOp = new JButton("Elimina operazione");
        this.btnDeleteOp.setBounds(WC4, HC1, WC, HC);
        panelSouth.add(btnDeleteOp);
        //BOTTONE SVUOTA VARRELLO
        this.btnEmptyCart = new JButton("Svuota il carrello");
        this.btnEmptyCart.setBounds(WC5, HC1, WC, HC);
        panelSouth.add(btnEmptyCart);
        //LABEL PREZZO
        this.labPrice = new JLabel();
        this.labPrice.setBounds(WC2, HC2, WC, HC);
        this.labPrice.setFont(new Font("Tahoma", Font.PLAIN, FONT2));
        panelSouth.add(labPrice);
        //BOTTONE CONCLUDI ACQUISTO
        this.btnFinishOp = new JButton("Concludi l'acquisto");
        this.btnFinishOp.setBounds(WC4, HC2, WC, HC);
        panelSouth.add(btnFinishOp);
        //BOTTONE PAGINA PRECEDENTE
        this.btnRetMenu = new JButton("Pagina precedente");
        this.btnRetMenu.setBounds(WC5, HC2, WC, HC);
        panelSouth.add(btnRetMenu);
        final Component c1 = panelSouth;
        lim.gridx = 0;
        lim.gridy = 1;
        lim.weightx = 1;
        lim.weighty = LIM;
        lim.ipadx = 0;
        lim.ipady = LIM;
        lim.fill = GridBagConstraints.HORIZONTAL;
        lim.anchor = GridBagConstraints.CENTER;
        layout.setConstraints(c1, lim);
        this.add(c1);
        panelSouth.setLayout(null);
        this.setVisible(false);
    }
    @Override
    public void addTableRow(final Integer num,  final Pair<String, Pair<String, String>> price) {
        model.addRow(new Object[]{num, price.getY().getX(), price.getY().getY(), price.getX() + " Euro"});
        this.count++;
    }
    @Override
    public void deleteOp(final Integer i) {
        model.removeRow(i - 1);
    }
    @Override
    public void deleteTable() {
        for (int i = this.count - 1; i >= 0; i--) {
            model.removeRow(i);
        }
        this.count = 0;
    }
    @Override
    public void addLabelPrice(final String price) {
        this.labPrice.setText(price + " Euro");
    }
    @Override
    public JPanel getPanel() {
        return this;
    }
    @Override
    public JButton getBtnPrev() {
        return this.btnRetMenu;
    }
    @Override
    public JButton getBtnFinishOp() {
        return this.btnFinishOp;
    }
    @Override
    public JButton getBtnDeleteCart() {
        return this.btnEmptyCart;
    }
    @Override
    public JButton getBtnDeleteOperation() {
        return this.btnDeleteOp;
    }
    @Override
    public String getOperationDelete() {
        return this.text.getText();
    }
    @Override
    public JTextField getOp() {
        return this.text;
    }

}
