package view.panels.store;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import controller.Controller;
import model.admin.products.BuyObject;
import view.panels.FrameSize;
import view.panels.interfaces.PanelBuy;

/**
 * 
 * panel buy class.
 *
 */
public class PanelBuyImpl extends JPanel implements PanelBuy {
    private static final long serialVersionUID = 1L;
    private final JButton btnRetMenu;
    private final JButton btnCart;
    private final Map<JButton, BuyObject> jbMap;
    //COLORE
    private static final int C1 = 171;
    private static final int C2 = 205;
    private static final int C3 = 239;
    private static final int FONT_LAB = FrameSize.WIDTH.getValue() / 40;

    private static final int WC1 = FrameSize.WIDTH.getValue() / 8;
    private static final int HC3 = FrameSize.HEIGHT.getValue() / 6;

    /**
     * panel buy constructor.
     */
    public PanelBuyImpl() {
        super();
        final Controller ci = Controller.getController();
        this.setLayout(new BorderLayout());
        final TitledBorder titleBorder = new TitledBorder("Scegli il prodotto che vuoi acquistare");
        titleBorder.setTitleJustification(TitledBorder.CENTER);
        titleBorder.setTitleColor(Color.RED);
        titleBorder.setTitleFont(new Font(Font.SANS_SERIF, Font.BOLD, FONT_LAB));
        this.setBorder(titleBorder);
        final JPanel pc = new JPanel();
        final JPanel ps = new JPanel();
        this.add(pc, BorderLayout.CENTER);
        this.add(ps, BorderLayout.SOUTH);
        pc.setLayout(new GridLayout(3, 4));
        this.jbMap = new HashMap<>();

        for (final BuyObject obj : ci.getBuyObjects()) {
            final ImageIcon icon = new ImageIcon(getClass().getResource(obj.getImage()));
            final JButton bt = new JButton(icon);
            final JLabel jl = new JLabel(obj.getDescription());
            final Image scaledImage = icon.getImage().getScaledInstance(WC1, HC3, Image.SCALE_DEFAULT);
            icon.setImage(scaledImage);
            this.jbMap.put(bt, obj);
            pc.add(bt);
            bt.setOpaque(false);
            bt.setContentAreaFilled(false);
            bt.setBorderPainted(false);
            bt.add(jl);
            jl.setAlignmentY(JLabel.BOTTOM_ALIGNMENT);
        }
        final ImageIcon imgCart = new ImageIcon(getClass().getResource("/carrello.gif"));
        this.btnCart = new JButton(imgCart);
        final Image scaledImage = imgCart.getImage().getScaledInstance(FrameSize.WIDTH.getValue() / 12, FrameSize.HEIGHT.getValue() / 10, Image.SCALE_DEFAULT);
        imgCart.setImage(scaledImage);
        this.btnRetMenu = new JButton("Pagina precedente");
        this.btnRetMenu.setFont(new Font("Tahoma", Font.PLAIN, FONT_LAB));
        ps.add(btnRetMenu);
        ps.add(btnCart);
        btnCart.setOpaque(false);
        btnCart.setContentAreaFilled(false);
        btnCart.setBorderPainted(false);
        final Color bluette = new Color(C1, C2, C3);
        pc.setBackground(bluette);
        ps.setBackground(bluette);
        this.setBackground(bluette);
        this.setVisible(false);
    }
    @Override
    public JPanel getPanel() {
        return this;
    }
    @Override
    public JButton getBtnPrev() {
        return this.btnRetMenu;
    }
    @Override
    public JButton getBtnCart() {
        return this.btnCart;
    }
    @Override
    public Map<JButton, BuyObject> getBtnObj() {
        return this.jbMap;
    }
}
