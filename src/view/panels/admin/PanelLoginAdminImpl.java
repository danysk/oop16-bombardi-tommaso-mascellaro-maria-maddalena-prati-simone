package view.panels.admin;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import view.panels.FrameSize;
import view.panels.interfaces.PanelLoginAdmin;
/**
 * 
 * panel login admin class.
 *
 */
public class PanelLoginAdminImpl extends JPanel implements PanelLoginAdmin {
    private static final long serialVersionUID = 1L;
    private final JTextField password;
    private final JTextField username;
    private final JButton login;
    private final JButton btnReturnMenu;

    private static final int W1 = FrameSize.WIDTH.getValue() / 15;
    private static final int W2 = FrameSize.WIDTH.getValue() / 6;
    private static final int W3 = FrameSize.WIDTH.getValue() / 2;
    private static final int W4 = FrameSize.WIDTH.getValue() / 4;
    private static final int W5 = (int) (FrameSize.WIDTH.getValue() / 1.5);
    private static final int W6 = FrameSize.WIDTH.getValue() / 5;
    private static final int H1 = FrameSize.HEIGHT.getValue() / 25;
    private static final int H2 = FrameSize.HEIGHT.getValue() / 15;
    private static final int H3 = FrameSize.HEIGHT.getValue() / 3;
    private static final int H4 = FrameSize.HEIGHT.getValue() / 5;
    private static final int H5 = FrameSize.HEIGHT.getValue() / 20;
    private static final int H6 = (int) (FrameSize.HEIGHT.getValue() / 1.3);
    private static final int H7 = FrameSize.HEIGHT.getValue() / 10;

    private static final int LAB = FrameSize.WIDTH.getValue() / 20;
    private static final int LAB1 = FrameSize.WIDTH.getValue() / 25;

    /**
     * panel login admin constructor.
     */
    public PanelLoginAdminImpl() {
        super();
        //SFONDO
        final ImageIcon img = new ImageIcon(getClass().getResource("/sfondo.jpg"));
        final Image scaledImage = img.getImage().getScaledInstance(FrameSize.WIDTH.getValue(), FrameSize.HEIGHT.getValue(), Image.SCALE_DEFAULT);
        img.setImage(scaledImage);
        final JLabel labelImg = new JLabel(img);
        labelImg.setBounds(0, 0, FrameSize.WIDTH.getValue(), FrameSize.HEIGHT.getValue());
        //TITOLO
        final JLabel labLoginAdmin = new JLabel("LOGIN ADMIN");
        labLoginAdmin.setBounds(W1, H1, FrameSize.WIDTH.getValue(), H2);
        labLoginAdmin.setFont(new Font("Tahoma", Font.PLAIN, LAB));
        labLoginAdmin.setForeground(Color.RED);
        labelImg.add(labLoginAdmin);
        //USER
        final JLabel labusername = new JLabel("Username");
        labusername.setBounds(W2, H4, W4, H5);
        labusername.setFont(new Font("Tahoma", Font.PLAIN, LAB1));
        labusername.setForeground(Color.RED);
        labelImg.add(labusername);
        this.username = new JTextField();
        this.username.setBounds(W3, H4, W4, H5);
        labelImg.add(username);
        //PASSWORD
        final JLabel labpassword = new JLabel("Password");
        labpassword.setBounds(W2, H3, W4, H5);
        labpassword.setFont(new Font("Tahoma", Font.PLAIN, LAB1));
        labpassword.setForeground(Color.RED);
        labelImg.add(labpassword);
        this.password = new JPasswordField();
        this.password.setBounds(W3, H3, W4, H5);
        labelImg.add(password);
        //BOTTONE LOGIN
        this.login = new JButton("Login");
        this.login.setBounds(W6, H6, W4, H7);
        labelImg.add(login);
        //BOTTONE MENU
        this.btnReturnMenu = new JButton("Torna al menu'");
        this.btnReturnMenu.setBounds(W5, H6, W4, H7);
        labelImg.add(btnReturnMenu);
        this.add(labelImg);
        this.setLayout(null);
        this.setVisible(false);
    }
    @Override
    public JPanel getPanel() {
        return this;
    }
    @Override
    public JTextField getUsername() {
        return this.username;
    }
    @Override
    public JTextField getPassword() {
        return this.password;
    }
    @Override
    public JButton getBtnLogin() {
        return this.login;
    }
    @Override
    public JButton getBtnPrev() {
        return this.btnReturnMenu;
    }

}
