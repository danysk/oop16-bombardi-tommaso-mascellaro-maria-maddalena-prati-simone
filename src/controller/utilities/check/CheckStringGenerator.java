package controller.utilities.check;

import java.util.Optional;

/**
 * Class created to generate check string functions, designed using strategy pattern.
 */
public class CheckStringGenerator implements CheckString {

    private final CheckString checkStr;

    /**
     * Constructor for check string generator.
     * 
     * @param check
     *              implementation of interface check string that will be used
     */
    public CheckStringGenerator(final CheckString check) {
        super();
        this.checkStr = check;
    }

    @Override
    public Optional<String> check(final CheckType type, final String str, final int min, final int max) {
        return this.checkStr.check(type, str, min, max);
    }

}