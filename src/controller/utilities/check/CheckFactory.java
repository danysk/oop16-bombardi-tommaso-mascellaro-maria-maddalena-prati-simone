package controller.utilities.check;

/**
 * Interface for check factory, created to use the check strategy pattern in the application.
 */
public interface CheckFactory {

    /**
     * Check number.
     * 
     * @return the check number generator that i will use in this application
     */
    CheckNumber getCheckNum();

    /**
     * Check string.
     * 
     * @return the check string generator that i will use in this application
     */
    CheckString getCheckStr();

}
