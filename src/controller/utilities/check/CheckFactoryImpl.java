package controller.utilities.check;

import java.util.Optional;

/**
 * Implementation of check factory, created to use the check strategy pattern in the application.
 */
public final class CheckFactoryImpl implements CheckFactory {

    private static final CheckFactory SINGLETON = new CheckFactoryImpl();

    /**
     * Private constructor for controller implementation.
     */
    private CheckFactoryImpl() {
        super();
    }

    /**
     * Get check factory.
     * 
     * @return the only one instance of check factory
     */
    public static synchronized CheckFactory getCheckFactory() {
        return SINGLETON;
    }

    @Override
    public CheckNumber getCheckNum() {
        return new CheckNumberGenerator(new CheckNumber() {
            @Override
            public Optional<Integer> check(final String num, final int max) {
                if (this.checkNumber(num).isPresent() && max != -1 && this.checkNumber(num).get() > max) {
                    return Optional.empty();
                } else {
                    return this.checkNumber(num);
                }
            }
            private Optional<Integer> checkNumber(final String num) {
                Optional<Integer> op = Optional.empty();
                if (this.checkInteger(num)) {
                     final Integer val = Integer.valueOf(num);
                     if (val >= 1) {
                         op = Optional.of(val);
                     }
                }
                return op;
            }
            private boolean checkInteger(final String num) {
                try {
                    Integer.parseInt(num);
                } catch (NumberFormatException n) {
                    return false;
                }
                return true;
            }
        });
    }

    @Override
    public CheckString getCheckStr() {
        return new CheckStringGenerator(new CheckString() {
            @Override
            public Optional<String> check(final CheckType type, final String str, final int min, final int max) {
                if (type.equals(CheckType.LENGTH)) {
                    return this.checkLength(str, min, max);
                } else if (type.equals(CheckType.NAME)) {
                    return this.checkName(str, min, max);
                } else {
                    return this.checkString(str, min, max);
                }
            }
            private Optional<String> checkName(final String str, final int min, final int max) {
                int charCount = 0;
                boolean check = true;
                for (int i = 0; i < str.length(); i++) {
                    final char c = str.charAt(i);
                    if (Character.isLetter(c)) {
                        charCount++;
                    } else {
                        if (!Character.isSpaceChar(c)) {
                            check = false;
                        }
                    }
                }
                if (charCount < min || charCount > max || !check) {
                    return Optional.empty();
                } else {
                    return Optional.of(str);
                }
            }
            private Optional<String> checkLength(final String str, final int min, final int max) {
                if (str.length() >= min && str.length() <= max) {
                    return Optional.of(str);
                } else {
                    return Optional.empty();
                }
            }
            private Optional<String> checkString(final String str, final int min, final int max) {
                if (str.isEmpty() || this.checkInteger(str)) {
                    return Optional.empty();
                } else {
                    return this.checkLength(str, min, max);
                }
            }
            private boolean checkInteger(final String num) {
                try {
                    Integer.parseInt(num);
                } catch (NumberFormatException n) {
                    return false;
                }
                return true;
            }
        });
    }

}