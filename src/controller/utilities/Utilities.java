package controller.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Optional;

import controller.utilities.check.CheckFactory;
import controller.utilities.check.CheckFactoryImpl;
import controller.utilities.check.CheckString.CheckType;;

/**
 * Class with utilities (check and save) implementation.
 */
public class Utilities implements Check, Save {

    private static final CheckFactory CF = CheckFactoryImpl.getCheckFactory();
    private static final int MAX_OBJECTS = 10;
    private static final int MAX_DAYS = 20;
    private static final int MAX_STUDENTS = 4;
    private static final int MIN_NAME_LETTERS = 3;
    private static final int MAX_NAME_LETTERS = 40;
    private static final int MIN_USER_LETTERS = 5;
    private static final int MAX_USER_LETTERS = 8;
    private static final int CARD_NUMBER_LENGTH = 16;
    private static final int CARD_DATE_LENGTH = 7;
    private static final int CARD_CVC_LENGTH = 3;

    @Override
    public Optional<Integer> checkObjects(final String num) {
        return CF.getCheckNum().check(num, MAX_OBJECTS);
    }
    @Override
    public Optional<Integer> checkDays(final String num) {
        return CF.getCheckNum().check(num, MAX_DAYS);
    }
    @Override
    public Optional<Integer> checkStudents(final String num) {
        return CF.getCheckNum().check(num, MAX_STUDENTS);
    }
    @Override
    public Optional<Integer> checkIndex(final String num) {
        return CF.getCheckNum().check(num, -1);
    }
    @Override
    public Optional<String> checkName(final String str) {
        return CF.getCheckStr().check(CheckType.NAME, str, MIN_NAME_LETTERS, MAX_NAME_LETTERS);
    }
    @Override
    public Optional<String> checkUsers(final String str) {
        return CF.getCheckStr().check(CheckType.STRING, str, MIN_USER_LETTERS, MAX_USER_LETTERS);
    }
    @Override
    public Optional<String> checkCardNumber(final String str) {
        return CF.getCheckStr().check(CheckType.LENGTH, str, CARD_NUMBER_LENGTH, CARD_NUMBER_LENGTH);
    }
    @Override
    public Optional<String> checkCardDate(final String str) {
        return CF.getCheckStr().check(CheckType.LENGTH, str, CARD_DATE_LENGTH, CARD_DATE_LENGTH);
    }
    @Override
    public Optional<Integer> checkCardCvc(final String num) {
        if (CF.getCheckStr().check(CheckType.LENGTH, num, CARD_CVC_LENGTH, CARD_CVC_LENGTH).isPresent()) {
            return CF.getCheckNum().check(num, -1);
        } else {
            return Optional.empty();
        }
    }


    @Override
    public File createFile(final File f) {
        if (f.exists()) {
            if (!f.isFile()) {
                try {
                    if (!f.delete()) {
                        throw new IllegalStateException();
                    }
                    if (!f.createNewFile()) {
                        throw new IllegalStateException();
                    }
                } catch (SecurityException se) {
                    throw new IllegalStateException("Security error", se);
                } catch (IOException e) {
                    throw new IllegalStateException("Input output error", e);
                }
            }
        } else {
            try {
                f.createNewFile();
            } catch (IOException e) {
                throw new IllegalStateException("Input output error", e);
            }
        }
        return f;
    }
    @Override
    public File createDir(final File d) {
        if (d.exists()) {
            if (!d.isDirectory()) {
                try {
                    if (!d.delete()) {
                        throw new IllegalStateException();
                    }
                    if (!d.mkdir()) {
                        throw new IllegalStateException();
                    }
                } catch (SecurityException se) {
                    throw new IllegalStateException("Security error", se);
                }
            }
        } else {
            try {
                d.mkdir();
            } catch (SecurityException se) {
                throw new IllegalStateException("Security error", se);
            } 
        }
        return d;
    }
    @Override
    public Optional<File> writeFile(final Object obj, final File f) {
        if (!f.exists()) {
            try {
                this.createFile(f);
            } catch (IllegalStateException e) {
                return Optional.empty();
            }
        }
        Optional<File> file;
        try {
            final ObjectOutputStream fileWriter = new ObjectOutputStream(new FileOutputStream(f));
            fileWriter.writeObject(obj);
            file = Optional.of(f);
            fileWriter.close();
        } catch (IOException e) {
            file = Optional.empty();
        }
        return file;
    }
    @Override
    public Optional<Object> readFile(final File f) {
        Optional<Object> obj;
        try {
            final ObjectInputStream fileReader = new ObjectInputStream(new FileInputStream(f));
            obj = Optional.of(fileReader.readObject());
            fileReader.close();
        } catch (IOException e1) {
            obj = Optional.empty();
        } catch (ClassNotFoundException e) {
            obj = Optional.empty();
        }
        return obj;
    }

}
